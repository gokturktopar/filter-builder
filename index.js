"use strict";
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const isNull=(data)=>{
    if (data==='null' || data==='undefined' || data === undefined  || data === null|| Number.isNaN(data) || data==='') 
        return true
    else 
        return false       
}
module.exports = (data,filter,filterType,fieldN)=> {
       /**
         * filterType
         * 0->string
         * 1->int
         * 2->id
         * 3->bool
         * 4->int array
         * 5->string array
         * 6->bool array
         * 7->start date 
         * 8->end date 
         * 9->id array
         * 11 ->int less than
         * 12 ->int greater than
         * 13-> search text
         */
        if (isNull(data)) 
                return filter
        if(filterType==0)
            filter.push({[fieldN]:data})
        else if(filterType==1)
            filter.push({[fieldN]:parseFloat(data)})
        else if (filterType==2) 
             filter.push({[fieldN]:ObjectId(data)})
        else if(filterType==3)
            filter.push({[fieldN]:data==='true'||data===true})
        else if(filterType==4){
            data=data.split(',')
            const values=[]
            for (let i = 0; i < data.length; i++) {
                if(parseFloat(data[i]))
                    values.push({[fieldN]:parseFloat(data[i])})
                  }
                filter.push({$or:values})
              }
        else if(filterType==5){
            data=data.split(',')
            const values=[]
            for (let i = 0; i < data.length; i++) {
                if(data[i]!=','&&data[i]!='['&&data[i]!=']')
                    values.push({[fieldN]:data[i]})
              }
              filter.push({$or:values})
            }
        else if(filterType==6){
            data=data.split(',')
            const values=[]
              for (let i = 0; i < data.length; i++) {
                if(data[i]!=','&&data[i]!='['&&data[i]!=']')
                    values.push({[fieldN]:data[i]==='true'||data[i]===true})
              }
              filter.push({$or:values})
          }
        else if(filterType==7){
            filter.push({[fieldN]:{$gte:new Date(data)}})
        }
        else if(filterType==8){
            filter.push({[fieldN]:{$lte:new Date(data)}})
        }
        else if(filterType==9){
            data=data.split(',')
            const values=[]
            for (let i = 0; i < data.length; i++) {
                if(ObjectId(data[i]))
                    values.push({[fieldN]:ObjectId(data[i])})
                }
            filter.push({$or:values})
        }
        else if(filterType==11){
            filter.push({[fieldN]:{$lte:parseFloat(data)}})
        }
        else if(filterType==12){
            filter.push({[fieldN]:{$gte:parseFloat(data)}})
        }
        else if(filterType==13){
            filter.push({ [fieldN]: {'$regex': data, '$options' : 'i'}})
        }
    return filter
        
    }