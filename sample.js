const filterBuilder=require('./index')


router.get('/',(req,res,next)=>{
    let filters=[]
    filters=filterBuilder(req.query.username,filters,0,'username')
    filters=filterBuilder(req.query.userId,filters,1,'_id')
    filters=filterBuilder(req.query.salary,filters,2,'salary')
    filters=filterBuilder(req.query.isActive,filters,3,'_id')
    filters=filterBuilder(req.query.salaries,filters,4,'salary')
    filters=filterBuilder(req.query.usernames,filters,5,'username')
    filters=filterBuilder(req.query.isActiveOptions,filters,6,'_id')
    filters=filterBuilder(req.query.birthdate,filters,7,'birthdate')
    filters=filterBuilder(req.query.birthdate,filters,8,'birthdate')
    filters=filterBuilder(req.query.userIds,filters,9,'_id')
    filters=filterBuilder(req.query.minSalary,filters,11,'salary')
    filters=filterBuilder(req.query.maxSalary,filters,12,'salary')
    filters=filterBuilder(req.query.username,filters,13,'username')
    if(filters.length>0)
        filters={$and:filters}
    else
        filters={} 
     const users=await UserModel.find(filters)
        res.json(users)

});