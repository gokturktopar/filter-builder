# MongoDb Filter Builder

Filter Builder is tool that parse incoming query string and build a filter object for using in MongoDB queries. Function takes  4 parameters which are query string data, filter object, one of filter type below stated and field name in database schema .

### Supported Filter Types
| Code | Filter Type |
| ------ | ------ |
| 0 | string |
| 1 | number |
| 2| id |
| 3 | bool |
| 4 | int array |
| 5 | string array |
| 6| bool array |
| 7 | start date |
| 8 | end date |
| 9 | id array |
| 11| int less than |
| 12 | int greater than |
| 13 | search text |


### Usage

This section demonstrate usage of builder . Let's assume we have a basic user document in our database.

| Field | Type |            
| ------ | ------ |
| _id | ObjectId |
| username | String |
| salary| Number |
| birthdate | Date |
| isActive | Boolean |


 #### Sample calls according to database schema.

| Query String | Sample Function |            
| ------ | ------ |
| /users?username=gokturk | filterBuilder(req.query.username,filters,0,'username')) |
| /users?salary=3000 | filterBuilder(req.query._id,filters,1,'salary')) |
| /users?userId=123123 | filterBuilder(req.query._id,filters,2,'_id')) |
| /users?isActive=true | filterBuilder(req.query.isActive,filters,2,'isActive')) |
| /users?minSalary=true | filterBuilder(req.query.salary,filters,11,'salary')) |

License
----

MIT


